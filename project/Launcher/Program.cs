﻿using System;
using System.Reflection;
using System.Windows.Forms;

namespace Launcher
{
    static class Program
    {

        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main());
        }

        public static byte[] GetEmbeddedFile(string filename)
        {
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("Launcher." + filename))
            {
                byte[] assemblyData = new byte[stream.Length];
                stream.Read(assemblyData, 0, assemblyData.Length);
                return assemblyData;
            }
        }
    }
}
