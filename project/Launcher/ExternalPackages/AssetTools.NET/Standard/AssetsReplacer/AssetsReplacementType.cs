﻿#if GAME_IS_RAFT
namespace AssetsTools.NET
{
    public enum AssetsReplacementType
    {
        AssetsReplacement_AddOrModify,
        AssetsReplacement_Remove
    }
}
#endif