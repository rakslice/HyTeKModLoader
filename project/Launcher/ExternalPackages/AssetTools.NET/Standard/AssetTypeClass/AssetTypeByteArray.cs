﻿#if GAME_IS_RAFT
namespace AssetsTools.NET
{
    public struct AssetTypeByteArray
    {
        public uint size;
        public byte[] data;
    }
}
#endif