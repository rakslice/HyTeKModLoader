﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GHML
{
    public class DefaultConsoleCommands
    {

        [ConsoleCommand("unityVersion", "Shows you the unity version the game use.")]
        public static string UnityVersion()
        {
            return "Current Unity Version: " + Application.unityVersion;
        }

        [ConsoleCommand("timeset", "Syntax: 'timeset <hour>' Change the game time.")]
        public static void Timeset(string[] args)
        {
            if (SceneManager.GetActiveScene().name == "MainMenu")
            {
                Debug.LogWarning("You can't set the time in the main menu.");
                return;
            }
            if (args.Length != 1)
            {
                Debug.LogWarning("You must specify a value to change the game time. Syntax: 'timeset <hour>'");
            }
            else
            {
                int newValue = -1;
                int.TryParse(args[0], out newValue);

                if (newValue < 0 || newValue > 24)
                {
                    Debug.LogWarning("The value that you specified is invalid, It should be between 0 and 24!");
                    return;
                }
                else
                {
                    MainLevel.Instance.m_TODSky.Cycle.Hour = newValue;
                    Debug.Log("The game time has been changed to " + newValue);
                }

            }
        }

        [ConsoleCommand("ghVersion", "Shows you the current Green Hell Version.")]
        public static string GreenhellVersion()
        {
            return "Current Green Hell Version: " + GreenHellGame.s_GameVersion.WithBuildVersionToString();
        }

        [ConsoleCommand("dotnetVersion", "Shows you the .NET version the game use.")]
        public static string dotnetVersion()
        {
            return "Current .NET Version: " + Environment.Version.ToString();
        }

        [ConsoleCommand("clear", "Clear the current console output.")]
        public static void clear()
        {
            GHConsole.ClearConsole();
        }

        [ConsoleCommand("kill", "Kill yourself.")]
        public static void kill()
        {
            if (Player.Get() != null)
            {
                Player.Get().Kill();
                Debug.Log("The suicide note was written in blue ink.");
                return;
            }
            Debug.LogWarning("You can't suicide right now.");
        }

        [ConsoleCommand("exit", "Exit the game.")]
        public static void exit()
        {
            Application.Quit();
        }

        [ConsoleCommand("getcurrentlevel", "Tells you the current game level.")]
        public static string getcurrentlevel()
        {
            return "Current Level : " + SceneManager.GetActiveScene().name;
        }

        [ConsoleCommand("loadlevel", "Syntax: 'loadlevel <value>' Load the specified level.")]
        public static void loadlevel(string[] args)
        {
            if (args.Length != 1)
            {
                Debug.LogWarning("Invalid Arguments! Syntax: 'loadlevel <value>'");
                return;
            }
            else
            {
                SceneManager.LoadScene(args[0]);
            }
        }

        [ConsoleCommand("poopmode", "Makes the game looks like minecraft but it helps to play the game on low-end devices such as old laptops.")]
        public static void PoopMode()
        {
            QualitySettings.pixelLightCount = 0;
            QualitySettings.masterTextureLimit = 2;
            QualitySettings.anisotropicFiltering = AnisotropicFiltering.Disable;
            QualitySettings.antiAliasing = 0;
            QualitySettings.softParticles = false;
            QualitySettings.realtimeReflectionProbes = false;
            QualitySettings.billboardsFaceCameraPosition = false;
            QualitySettings.shadowCascades = 0;
            QualitySettings.shadows = ShadowQuality.Disable;
            QualitySettings.softVegetation = false;
            QualitySettings.vSyncCount = 0;
            //QualitySettings.lodBias = 0;
            QualitySettings.maximumLODLevel = 1;
            QualitySettings.shadowDistance = 0;
            QualitySettings.maxQueuedFrames = 1000;

            Application.backgroundLoadingPriority = ThreadPriority.Low;
            Application.runInBackground = true;
        }
    }
}