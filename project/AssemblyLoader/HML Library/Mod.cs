﻿using UnityEngine;
using HMLLibrary;

public class Mod : MonoBehaviour
{
    private static ModData listReference;

    public Mod()
    {
        listReference = ModManagerPage.modList.Find(t => t.modinfo.assembly == GetType().Assembly);
    }

    public virtual void UnloadMod()
    {
        listReference.modinfo.modHandler.UnloadMod(listReference);
    }

    public virtual byte[] GetEmbeddedFileBytes(string path)
    {
        byte[] value = null;
        listReference.modinfo.modFiles.TryGetValue(path, out value);
        if (value == null)
            Debug.LogError("[ModManager] " + listReference.jsonmodinfo.name + " > The file " + path + " doesn't exist!");
        return value;
    }

    public JsonModInfo GetModInfo() { return listReference.jsonmodinfo; }

#if GAME_IS_RAFT
    public virtual void WorldEvent_WorldLoaded()
    {
    }

    public virtual void WorldEvent_WorldSaved()
    {
    }

    public virtual void LocalPlayerEvent_Hurt(float damage, Vector3 hitPoint, Vector3 hitNormal, EntityType damageInflictorEntityType)
    {
    }

    public virtual void LocalPlayerEvent_Death(Vector3 deathPosition)
    {
    }

    public virtual void LocalPlayerEvent_Respawn()
    {
    }

    public virtual void LocalPlayerEvent_ItemCrafted(Item_Base item)
    {
    }

    public virtual void LocalPlayerEvent_PickupItem(PickupItem item)
    {
    }

    public virtual void LocalPlayerEvent_DropItem(ItemInstance item, Vector3 position, Vector3 direction, bool parentedToRaft)
    {
    }
#endif
}