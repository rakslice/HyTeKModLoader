﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using UnityEngine;

using Debug = UnityEngine.Debug;

namespace HMLLibrary
{
    public class TemporaryRawModHandler : BaseModHandler
    {
        public override async Task<ModData> GetModData(FileInfo file)
        {
            ModData moddata = new ModData();
            moddata.jsonmodinfo.name = file.Name;
            moddata.modinfo.modHandler = this;
            moddata.jsonmodinfo.author = "Update Me!";
            moddata.jsonmodinfo.version = "Update Me!";
            moddata.modinfo.modFile = file;
            moddata.modinfo.fileHash = HUtils.GetFileSHA512Hash(file.FullName);
            moddata.jsonmodinfo.isModPermanent = true;
            return moddata;
        }

        public override async Task<Assembly> GetModAssembly(ModData moddata)
        {
            Assembly assembly = null;
            try
            {
                moddata.modinfo.modState = ModInfo.ModStateEnum.compiling;
                ModManagerPage.RefreshModState(moddata);
                StringBuilder cmdArguments = new StringBuilder();
                string outPath = string.Format("{0}\\modassembly_{1}.dll", HLib.path_cacheFolder_temp, DateTime.Now.Ticks);
                List<string> assemblies = new List<string>();
                string path = Path.Combine(Application.dataPath, "../" + HLib.datafoldername + "/Managed");
                assemblies = Directory.GetFiles(path).Where(file => file.EndsWith(".dll") && !file.Contains("System.")).ToList();

                foreach (string dllFile in Directory.GetFiles(HLib.path_binariesFolder).Where(b => b.ToLower().EndsWith(".dll")))
                {
                    assemblies.Add(dllFile);
                }
                string fileContent = File.ReadAllText(moddata.modinfo.modFile.FullName);
                if (ModManager.DoesModNeedsToBeRepaired(fileContent))
                {
                    HNotification notification = HNotify.Get().AddNotification(HNotify.NotificationType.spinning, 0);
                    notification.SetText("Repairing mod " + moddata.jsonmodinfo.name + "...");
                    File.WriteAllText(moddata.modinfo.modFile.FullName, ModManager.RepairMod(moddata, fileContent));
                    notification.Close();
                }

                cmdArguments.Append(string.Format("\"{0}\"", assemblies[0]));
                for (int i = 1; i < assemblies.Count; i++)
                {
                    cmdArguments.Append(string.Format(",\"{0}\"", assemblies[i]));
                }
                Process process = new Process();
                process.StartInfo.UseShellExecute = false;
                process.StartInfo.CreateNoWindow = true;
                process.StartInfo.FileName = ModManager.cscPath;
                process.StartInfo.Arguments = string.Format("/r:{0} /out:\"{1}\" /preferreduilang:en-US /target:library /nostdlib \"{2}\"", cmdArguments.ToString(), outPath, moddata.modinfo.modFile.FullName);
                process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                process.StartInfo.RedirectStandardOutput = true;
                process.Start();
                while (process.HasExited != true) { await Task.Delay(1); }
                string output = process.StandardOutput.ReadToEnd();
                string[] outputFixed = output.Split(new string[] { "http://go.microsoft.com/fwlink/?LinkID=533240" }, StringSplitOptions.None);
                try
                {
                    assembly = Assembly.Load(File.ReadAllBytes(outPath));
                }
                catch (Exception e)
                {
                    outputFixed[1] = Regex.Replace(outputFixed[1], @"^\s+$[\r\n]*", string.Empty, RegexOptions.Multiline);
                    if (!File.Exists(outPath))
                    {
                        string error = outputFixed[1].TrimEnd('\n');
                        Debug.LogError("[ModCompiler] " + moddata.modinfo.modFile.Name + " > The mod failed to compile!\n" + error);
                    }
                    else
                    {
                        string error = e.ToString();
                        if (e.InnerException != null)
                        {
                            error = e.InnerException.Message;
                        }
                        Debug.LogError("[ModCompiler] " + moddata.modinfo.modFile.Name + " > The mod failed to compile!\n" + error.TrimEnd('\n'));
                    }
                }
                if (File.Exists(outPath))
                {
                    File.Delete(outPath);
                }
            }
            catch (Exception e)
            {
                Debug.LogError("[ModCompiler] " + moddata.modinfo.modFile.Name + "> The mod failed to compile!\n" + e.ToString().TrimEnd('\n'));
            }
            return assembly;
        }
    }
}