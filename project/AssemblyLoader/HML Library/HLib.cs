﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace HMLLibrary
{
    public class HLib
    {
#if GAME_IS_RAFT
        public static string path_dataFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "RaftModLoader");
        public static string datafoldername = "Raft_Data";
#elif GAME_IS_GREENHELL
        public static string datafoldername = "GH_Data";
        public static string path_dataFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "GreenHellModLoader");
#endif
        public static string path_binariesFolder = Path.Combine(path_dataFolder, "binaries");
        public static string path_logsFolder = Path.Combine(path_dataFolder, "logs");
        public static string path_cacheFolder = Path.Combine(path_dataFolder, "cache");
        public static string path_cacheFolder_mods = Path.Combine(path_cacheFolder, "mods");
        public static string path_cacheFolder_textures = Path.Combine(path_cacheFolder, "textures");
        public static string path_cacheFolder_temp = Path.Combine(path_cacheFolder, "temp");
        public static string path_configFile = Path.Combine(path_dataFolder, "launcher_config.json");
        public static string path_modsFolder = Path.Combine(Application.dataPath, "..\\mods");
        public static string gameLogFile = Path.Combine(path_logsFolder, "coremod.log");
        public static Texture2D missingTexture;
        public static AssetBundle bundle;
    }

}
