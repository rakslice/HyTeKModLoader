﻿using HMLLibrary;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;
using UnityEngine;

namespace AssemblyLoader
{
    public class Loader
    {
        static GameObject gameObject;
#if GAME_IS_GREENHELL
        public static int assembliesCount = 85;
        public static string folderName = "GreenHellModLoader";
        public static string mainClass = "GHML_Main";
        public static string website = "https://www.greenhellmodding.com/";
        public static string modFormat = ".ghmod";
        public static string settingsPrefix = "ghmlSettings_";
#elif GAME_IS_RAFT
        public static int assembliesCount = 79;
        public static string folderName = "RaftModLoader";
        public static string mainClass = "RML_Main";
        public static string website = "https://www.raftmodding.com/";
        public static string modFormat = ".rmod";
        public static string settingsPrefix = "rmlSettings_";
#endif

        public static List<string> embedded_assemblies = new List<string>()
        {
            "0Harmony.dll",
            "SharpZipLib.dll"
        };

        public async static void Load()
        {
            while (AppDomain.CurrentDomain.GetAssemblies().Length < assembliesCount)
            {
                await Task.Delay(1500);
            }
            await Task.Delay(1500);
            gameObject = new GameObject();
            gameObject.AddComponent<HyTeKInjector>();
            GameObject.DontDestroyOnLoad(gameObject);
        }
    }

    public class HyTeKInjector : MonoBehaviour
    {
        [Serializable]
        public class LauncherConfiguration
        {
            public string gamePath = "";
            public string coreVersion = "?";
            public int agreeWithTOS = 0;
            public bool skipSplashScreen = false;
            public bool disableAutomaticGameStart = false;
        }

        public static string modloaderFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), Loader.folderName);
        public static string modloader_configfile = Path.Combine(modloaderFolder, "launcher_config.json");
        public static LauncherConfiguration modloader_config = null;

        void Awake()
        {
            Directory.CreateDirectory(modloaderFolder);
            Directory.CreateDirectory(Path.Combine(modloaderFolder, "logs"));

            if (File.Exists(modloader_configfile))
            {
                modloader_config = JsonUtility.FromJson<LauncherConfiguration>(File.ReadAllText(modloader_configfile));
                StartModLoader();
            }
        }

        public static void LoadEmbeddedAssemblies()
        {
            foreach (string s in Loader.embedded_assemblies)
            {
                try
                {
                    byte[] ba = GetAssemblyBytes("AssemblyLoader.Dependencies." + s, s);
                    Assembly asm = Assembly.Load(ba);
                    File.WriteAllBytes(Path.Combine(modloaderFolder, "binaries/" + s), ba);
                }
                catch { }
            }
        }

        public static void StartModLoader()
        {
            try
            {
                string logfilePath = Path.Combine(modloaderFolder, "logs\\modloader.log");
                File.WriteAllText(logfilePath, "");
                string dllFile = Path.Combine(modloaderFolder, "binaries/coremod.dll");
                if (!GameObject.Find("HyTeKModLoader") && !GameObject.Find("HyTeKDedicatedServer"))
                {
                    LoadEmbeddedAssemblies();
                    Assembly assembly = Assembly.Load(File.ReadAllBytes(dllFile));
                    ModLoaderLog("Loaded assembly : " + assembly.ToString());
                    GameObject gameObject = new GameObject("HyTeKModLoader");
                    GameObject.DontDestroyOnLoad(gameObject);
                    gameObject.AddComponent(assembly.GetType(Loader.mainClass));
                    ModLoaderLog("HyTeKModLoader has been successfully loaded!");
                    File.WriteAllText(Path.Combine(modloaderFolder, "launcher_config.json"), JsonUtility.ToJson(modloader_config, true));
                }
            }
            catch (Exception e)
            {
                ModLoaderLog("/!\\ StartModLoader() : A fatal error occured! /!\\\n" + e.ToString());
            }
        }

        public static byte[] GetAssemblyBytes(string embeddedResource, string fileName)
        {
            byte[] ba = null;
            Assembly curAsm = Assembly.GetExecutingAssembly();

            using (Stream stm = curAsm.GetManifestResourceStream(embeddedResource))
            {
                if (stm == null)
                    throw new Exception(embeddedResource + " is not found in Embedded Resources.");

                ba = new byte[(int)stm.Length];
                stm.Read(ba, 0, (int)stm.Length);
                if (ba.Length > 1000)
                {
                    return ba;
                }
            }
            return null;
        }

        public static void ModLoaderLog(string text)
        {
            string logfilePath = Path.Combine(modloaderFolder, "logs\\modloader.log");
            File.AppendAllText(logfilePath, text + "\n");
        }
    }
}