﻿using RaftModLoader;
using Steamworks;
using System;
using System.Collections.Generic;
using System.Linq;
using UltimateWater;
using UnityEngine;
using UnityEngine.AzureSky;
using UnityEngine.SceneManagement;

public class DefaultConsoleCommands : MonoBehaviour
{

    /*[ConsoleCommand("unityVersion", "Shows you the unity version the game use.")]
    public static string UnityVersion()
    {
        return "Current Unity Version: " + Application.unityVersion;
    }

    [ConsoleCommand("raftVersion", "Shows you the current Green Hell Version.")]
    public static string RaftVersion()
    {
        return "Current Raft Version: " + Settings.VersionNumberText;
    }

    [ConsoleCommand("dotnetVersion", "Shows you the current Green Hell Version.")]
    public static string DotNetVersion()
    {
        return "Current .NET Version: " + Environment.Version.ToString();
    }
    */
    //[ConsoleCommand("clear", "Clear the current console output.")]
    public static void clear()
    {
        RConsole.ClearConsole();
    }
    /*
    [ConsoleCommand("listRafters", "Shows you all your friends currently playing Raft.")]
    public static void ListRafters()
    {
        int friendCount = SteamFriends.GetFriendCount(EFriendFlags.k_EFriendFlagImmediate);
        List<string> rafters = new List<string>();
        for (int i = 0; i < friendCount; ++i)
        {
            CSteamID friendSteamId = SteamFriends.GetFriendByIndex(i, EFriendFlags.k_EFriendFlagImmediate);
            string friendName = SteamFriends.GetFriendPersonaName(friendSteamId);
            FriendGameInfo_t gameInfo;
            SteamFriends.GetFriendGamePlayed(friendSteamId, out gameInfo);
            if (gameInfo.m_gameID.ToString() == "648800")
            {
                rafters.Add(friendName + " is playing Raft! its SteamID is " + friendSteamId.GetAccountID().ToString());
            }
        }
        if (rafters.Count > 0)
        {
            Debug.Log("You have " + rafters.Count + " friend(s) currently playing Raft!");
            foreach (string s in rafters)
            {
                Debug.Log(s);
            }
        }
        else
        {
            Debug.Log("None of your friends are currently playing Raft!");
        }
    }

    [ConsoleCommand("connect", "Join a server, usage: <u><i>connect SteamID password</i></u>.")]
    public static void TryJoinServer(string[] args)
    {
        if (args.Length < 1)
        {
            Debug.LogWarning("Syntax error! Usage: <u><i>connect SteamID password</i></u>");
            return;
        }
        string steamid = args[0];
        string password = "";
        if (args.Length > 2)
        {
            args[0] = null;
            args[1] = null;
            password = String.Join(" ", args).TrimStart(' ');
        }

        try
        {
            if (steamid.Length > 5 && steamid.Length < 12)
            {
                uint accountid = uint.Parse(steamid);
                CSteamID csteamid = new CSteamID(new AccountID_t(accountid), EUniverse.k_EUniversePublic, EAccountType.k_EAccountTypeIndividual);
                if (!csteamid.IsValid())
                {
                    Debug.LogWarning("The provided SteamID is invalid, Valid SteamID's are SteamID,SteamID64,SteamID3 and AccountID");
                    return;
                }
                ServersPage.ConnectToServer(csteamid, password, false);
                return;
            }
        }
        catch { }
        RSocket.convertSteamid(steamid, password);
        return;
    }

    [ConsoleCommand("fullscreenMode", "Change the current fullscreen mode, usage: <u><i>fullscreenMode windowed/fullscreen/borderless</i></u>")]
    public static void clear()
    {
        RConsole.ClearConsole();
    }

    [ConsoleCommand("timeset", "Syntax: 'timeset <hour>' Change the game time.")]
    public static void Timeset(string[] args)
    {
        if (SceneManager.GetActiveScene().name == "MainMenu")
        {
            Debug.LogWarning("You can't set the time in the main menu.");
            return;
        }
        if (args.Length != 1)
        {
            Debug.LogWarning("You must specify a value to change the game time. Syntax: 'timeset <hour>'");
        }
        else
        {
            int newValue = -1;
            int.TryParse(args[0], out newValue);

            if (newValue < 0 || newValue > 24)
            {
                Debug.LogWarning("The value that you specified is invalid, It should be between 0 and 24!");
                return;
            }
            else
            {
                //MainLevel.Instance.m_TODSky.Cycle.Hour = newValue;
                Debug.Log("The game time has been changed to " + newValue);
            }

        }
    }

    [ConsoleCommand("dotnetVersion", "Shows you the .NET version the game use.")]
    public static string dotnetVersion()
    {
        return "Current .NET Version: " + Environment.Version.ToString();
    }

    [ConsoleCommand("kill", "Kill yourself.")]
    public static void kill()
    {
        if (RAPI.GetLocalPlayer() != null)
        {
            RAPI.GetLocalPlayer().Kill();
            Debug.Log("The suicide note was written in blue ink.");
            return;
        }
        Debug.LogWarning("You can't suicide right now.");
    }

    [ConsoleCommand("exit", "Exit the game.")]
    public static void exit()
    {
        Application.Quit();
    }

    [ConsoleCommand("getcurrentlevel", "Tells you the current game level.")]
    public static string getcurrentlevel()
    {
        return "Current Level : " + SceneManager.GetActiveScene().name;
    }

    [ConsoleCommand("loadlevel", "Syntax: 'loadlevel <value>' Load the specified level.")]
    public static void loadlevel(string[] args)
    {
        if (args.Length != 1)
        {
            Debug.LogWarning("Invalid Arguments! Syntax: 'loadlevel <value>'");
            return;
        }
        else
        {
            SceneManager.LoadScene(args[0]);
        }
    }

    [ConsoleCommand("poopmode", "Makes the game looks like minecraft but it helps to play the game on low-end devices such as old laptops.")]
    public static void PoopMode()
    {
        QualitySettings.pixelLightCount = 0;
        QualitySettings.masterTextureLimit = 2;
        QualitySettings.anisotropicFiltering = AnisotropicFiltering.Disable;
        QualitySettings.antiAliasing = 0;
        QualitySettings.softParticles = false;
        QualitySettings.realtimeReflectionProbes = false;
        QualitySettings.billboardsFaceCameraPosition = false;
        QualitySettings.shadowCascades = 0;
        QualitySettings.shadows = ShadowQuality.Disable;
        QualitySettings.softVegetation = false;
        QualitySettings.vSyncCount = 0;
        //QualitySettings.lodBias = 0;
        QualitySettings.maximumLODLevel = 1;
        QualitySettings.shadowDistance = 0;
        QualitySettings.maxQueuedFrames = 1000;

        Application.backgroundLoadingPriority = ThreadPriority.Low;
        Application.runInBackground = true;
    }*/

    public static void RegisterOldCommands()
    {
        RConsole.registerCommand(typeof(RML_Main), "Shows you the current unity Version.", "unityVersion", () => RConsole.Log("Current Unity Version: " + Application.unityVersion));
        RConsole.registerCommand(typeof(RML_Main), "Shows you the current Raft Version.", "raftVersion", () => RConsole.Log("Current Raft Version: " + Settings.VersionNumberText));
        RConsole.registerCommand(typeof(RML_Main), "Shows you the current .NET version.", "dotnetversion", () => RConsole.Log("Current .NET Version: " + Environment.Version.ToString()));
        RConsole.registerCommand(typeof(RML_Main), "Clear the current console output.", "clear", DefaultConsoleCommands.clear);
        RConsole.registerCommand(typeof(RML_Main), "Shows you all your friends currently playing Raft.", "listRafters", listRafters);
        RConsole.registerCommand(typeof(RML_Main), "Join a server, usage: <u><i>connect SteamID password</i></u>.", "connect", TryJoinServer);
        RConsole.registerCommand(typeof(RML_Main), "Change the current fullscreen mode, usage: <u><i>fullscreenMode windowed/fullscreen/borderless</i></u>", "fullscreenMode", changeFullscreen);
        RConsole.registerCommand(typeof(RML_Main), "Get the current roslyn compiler path.", "getcompiler", () => RawSharp.GetCSCPath());
        RConsole.registerCommand(typeof(RML_Main), "Syntax: 'settimescale <value>' Change the game speed (Default is 1).", "settimescale", settimescale);
        RConsole.registerCommand(typeof(RML_Main), "Kill yourself.", "kill", kill);
        RConsole.registerCommand(typeof(RML_Main), "Toggle the noclip.", "noclip", noclip);
        RConsole.registerCommand(typeof(RML_Main), "Teleports you to your raft.", "gotoraft", gotoraft);
        RConsole.registerCommand(typeof(RML_Main), "Exit the game.", "exit", () => Application.Quit());
        //RConsole.registerCommand(typeof(RML_Main), "Syntax: 'bind <key> <command>' Bind a console command to a key.", "bind", BindCommand);
        //RConsole.registerCommand(typeof(RML_Main), "Syntax: 'unbind <key>' Unbind a console comand from a key.", "unbind", UnbindCommand);
        //RConsole.registerCommand(typeof(RML_Main), "Unbind all commands from all keys.", "unbindall", Unbindall);
        RConsole.registerCommand(typeof(RML_Main), "Syntax: 'timeset <value>' Change the game time.", "timeset", timeset);
        RConsole.registerCommand(typeof(RML_Main), "Allows you to run csharp code at runtime.", "csrun", RawSharp.CSRUN);
        RConsole.registerCommand(typeof(RML_Main), "Makes the game looks like minecraft but it helps to play the game on low-end devices such as old laptops.", "poopmode", PoopMode);
        RConsole.registerCommand(typeof(RML_Main), "Toggle the RaftModLoader chat. (Temporarily fix for Russian & Chinese characters)", "togglecustomchat", RChat.ToggleCustomChat);
        //registerCommand(typeof(RML_Main), "Syntax: 'kick <username/steamid>' Eject a player from the game.", "kick", kick);
    }

    static void changeFullscreen()
    {
        if (RConsole.lcargs.Length != 2)
        {
            RConsole.LogWarning("Syntax error! Usage: <u><i>fullscreenMode windowed/fullscreen/borderless</i></u>");
            return;
        }
        string mode = RConsole.lcargs[1].ToLower();
        switch (mode)
        {
            case "windowed":
                Screen.SetResolution(Screen.width, Screen.height, FullScreenMode.Windowed);
                break;
            case "fullscreen":
                Screen.SetResolution(Screen.width, Screen.height, FullScreenMode.ExclusiveFullScreen);
                break;
            case "borderless":
                Screen.SetResolution(Screen.width, Screen.height, FullScreenMode.FullScreenWindow);
                break;
            default:
                RConsole.LogWarning("Invalid fullscreen mode, Valid modes are <i>Windowed</i>, <i>Fullscreen</i> or <i>Borderless</i>");
                break;
        }
    }

    static void settimescale()
    {
        RConsole.lcargs = RConsole.lcargs.Skip(1).ToArray();
        if (RConsole.lcargs.Length == 0)
        {
            RConsole.LogWarning("You must specify a value to change the game speed.");
            return;
        }

        RConsole.lcargs[0] = RConsole.lcargs[0].Replace(".", ",");
        float newValue = 1;
        float.TryParse(RConsole.lcargs[0], out newValue);

        if (newValue < 0 || newValue > 100)
        {
            RConsole.LogWarning("The value that you specified is invalid, It should be between 0 and 100!");
            return;
        }
        else
        {
            Time.timeScale = newValue;
            RConsole.Log("The game timescale has been changed to " + newValue);
        }
    }

    static void timeset()
    {
        RConsole.lcargs = RConsole.lcargs.Skip(1).ToArray();
        if (RConsole.lcargs.Length == 0)
        {
            RConsole.LogWarning("You must specify a value to change the game time.");
            return;
        }

        RConsole.lcargs[0] = RConsole.lcargs[0].Replace(".", ",");
        float newValue = 1;
        float.TryParse(RConsole.lcargs[0], out newValue);

        if (newValue < 0 || newValue > 24)
        {
            RConsole.LogWarning("The value that you specified is invalid, It should be between 0 and 24!");
            return;
        }
        else
        {
            FindObjectOfType<AzureSkyController>().timeOfDay.GotoTime(newValue);
            RConsole.Log("The game time has been changed to " + newValue);
        }
    }

    static void kill()
    {
        Network_Player ply = RAPI.GetLocalPlayer();
        if (ply != null && !ply.IsKilled && SceneManager.GetActiveScene().name == Semih_Network.GameSceneName)
        {
            ply.PlayerScript.Kill();
            RConsole.Log("The suicide note was written in blue ink.");
            return;
        }
        RConsole.LogWarning("You can't suicide right now.");
    }

    static void noclip()
    {
        Network_Player ply = RAPI.GetLocalPlayer();
        if (ply != null && !ply.IsKilled && SceneManager.GetActiveScene().name == Semih_Network.GameSceneName)
        {
            ply.flightCamera.Toggle(true);
            return;
        }
        RConsole.LogWarning("You can't use noclip right now.");
    }

    static void gotoraft()
    {
        Network_Player ply = RAPI.GetLocalPlayer();
        if (ply != null && !ply.IsKilled && SceneManager.GetActiveScene().name == Semih_Network.GameSceneName)
        {
            ply.PersonController.CameraSubmersionChanged(UltimateWater.SubmersionState.None, true);
            ply.PersonController.SwitchControllerType(ControllerType.Ground);
            ply.SetToWalkableBlockPosition();
            ply.PlayerScript.MakeUnStuck();
            return;
        }
        RConsole.LogWarning("You can't teleport to your raft right now.");
    }

    static void PoopMode()
    {
        QualitySettings.pixelLightCount = 0;
        QualitySettings.masterTextureLimit = 5;
        QualitySettings.anisotropicFiltering = AnisotropicFiltering.Disable;
        QualitySettings.antiAliasing = 0;
        QualitySettings.softParticles = false;
        QualitySettings.realtimeReflectionProbes = false;
        QualitySettings.billboardsFaceCameraPosition = false;
        QualitySettings.shadowCascades = 0;
        QualitySettings.shadows = ShadowQuality.Disable;
        QualitySettings.softVegetation = false;
        QualitySettings.blendWeights = BlendWeights.OneBone;
        QualitySettings.vSyncCount = 0;
        QualitySettings.lodBias = 0;
        QualitySettings.maximumLODLevel = 0;
        WaterQualitySettings.Instance.SetQualityLevel(0);
        QualitySettings.shadowDistance = 0;
        QualitySettings.maxQueuedFrames = 1000;
        Settings settings = FindObjectOfType<Settings>();
        settings.graphicsBox.postEffects.ambientOcclusion.enabled = false;
        settings.graphicsBox.postEffects.antialiasing.enabled = false;
        settings.graphicsBox.postEffects.bloom.enabled = false;
        settings.graphicsBox.postEffects.chromaticAberration.enabled = false;
        settings.graphicsBox.postEffects.colorGrading.enabled = false;
        settings.graphicsBox.postEffects.debugViews.enabled = false;
        settings.graphicsBox.postEffects.depthOfField.enabled = false;
        settings.graphicsBox.postEffects.dithering.enabled = false;
        settings.graphicsBox.postEffects.eyeAdaptation.enabled = false;
        settings.graphicsBox.postEffects.fog.enabled = false;
        settings.graphicsBox.postEffects.grain.enabled = false;
        settings.graphicsBox.postEffects.motionBlur.enabled = false;
        settings.graphicsBox.postEffects.screenSpaceReflection.enabled = false;
        settings.graphicsBox.postEffects.userLut.enabled = false;
        settings.graphicsBox.postEffects.vignette.enabled = false;

        FindObjectOfType<VolumetricLightRenderer>().Resolution = VolumetricLightRenderer.VolumtericResolution.Quarter;
        FindObjectOfType<VolumetricLightRenderer>().enabled = false;

        Application.backgroundLoadingPriority = ThreadPriority.Low;
        Application.runInBackground = true;
    }

    static void listRafters()
    {
        int friendCount = SteamFriends.GetFriendCount(EFriendFlags.k_EFriendFlagImmediate);
        List<string> rafters = new List<string>();
        for (int i = 0; i < friendCount; ++i)
        {
            CSteamID friendSteamId = SteamFriends.GetFriendByIndex(i, EFriendFlags.k_EFriendFlagImmediate);
            string friendName = SteamFriends.GetFriendPersonaName(friendSteamId);
            FriendGameInfo_t gameInfo;
            SteamFriends.GetFriendGamePlayed(friendSteamId, out gameInfo);
            if (gameInfo.m_gameID.ToString() == "648800")
            {
                rafters.Add(friendName + " is playing Raft! its SteamID is " + friendSteamId.GetAccountID().ToString());
            }
        }
        if (rafters.Count > 0)
        {
            RConsole.Log("You have " + rafters.Count + " friend(s) currently playing Raft!");
            foreach (string s in rafters)
            {
                RConsole.Log(s);
            }
        }
        else
        {
            RConsole.Log("None of your friends are currently playing Raft!");
        }
    }

    public static void TryJoinServer()
    {
        if (RConsole.lcargs.Length < 2 || RConsole.lastCommands.LastOrDefault().Length >= 50)
        {
            RConsole.LogWarning("Syntax error! Usage: <u><i>connect SteamID password</i></u>");
            return;
        }
        string steamid = RConsole.lcargs[1];
        string password = "";
        if (RConsole.lcargs.Length > 2)
        {
            RConsole.lcargs[0] = null;
            RConsole.lcargs[1] = null;
            password = String.Join(" ", RConsole.lcargs).TrimStart(' ');
        }

        try
        {
            if (steamid.Length > 5 && steamid.Length < 12)
            {
                uint accountid = uint.Parse(steamid);
                CSteamID csteamid = new CSteamID(new AccountID_t(accountid), EUniverse.k_EUniversePublic, EAccountType.k_EAccountTypeIndividual);
                if (!csteamid.IsValid())
                {
                    RConsole.LogWarning("The provided SteamID is invalid, Valid SteamID's are SteamID,SteamID64,SteamID3 and AccountID");
                    return;
                }
                ServersPage.ConnectToServer(csteamid, password, false);
                return;
            }
        }
        catch { }
        RSocket.convertSteamid(steamid, password);
        return;
    }
}