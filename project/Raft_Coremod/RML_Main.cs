﻿using HarmonyLib;
using HMLLibrary;
using RaftModLoader;
using SocketIO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using UnityEngine;
using Debug = UnityEngine.Debug;
using System.Linq;

public class RML_Main : MonoBehaviour
{
    private bool debug = false;

    public static List<string> logs = new List<string>();

    public static KeyCode MenuKey = KeyCode.F9;
    public static KeyCode ConsoleKey = KeyCode.F10;

    [Obsolete("Please use HLib.path_modsFolder instead")]
    public static string path_modsFolder = Path.Combine(Application.dataPath, "..\\mods");

    void OnEnable()
    {
        Application.logMessageReceived += HandleUnityLog;
    }
    void HandleUnityLog(string logString, string stackTrace, LogType type)
    {
        if (debug)
        {
            logs.Add(logString + "\n" + stackTrace);
        }
    }

    void OnGUI()
    {
        if (debug)
        {
            GUIStyle style = new GUIStyle();
            style.normal.textColor = Color.red;
            string text = "";
            foreach (string s in logs)
            {
                text += "\n" + s;
            }
            GUI.Label(new Rect(0, 0, Screen.width, Screen.height), text, style);
        }
    }

    void OnApplicationQuit()
    {
        Process.GetCurrentProcess().Kill();
    }

    private void Awake()
    {
        Application.backgroundLoadingPriority = ThreadPriority.High;
        Directory.CreateDirectory(HLib.path_dataFolder);
        Directory.CreateDirectory(HLib.path_cacheFolder);
        Directory.CreateDirectory(HLib.path_cacheFolder_mods);
        Directory.CreateDirectory(HLib.path_cacheFolder_textures);
        Directory.CreateDirectory(HLib.path_cacheFolder_temp);
        Directory.SetCurrentDirectory(Path.Combine(Application.dataPath, "..\\"));

        // Increment each time an update needs to clear the cache
        // I was really lazy to do something with datetime lmao
        int doCacheNeedsToBeCleared = 2;
        if (!PlayerPrefs.HasKey("rmlSettings_lastForcedCacheClearing"))
        {
            ForceCacheClearing();
            PlayerPrefs.SetInt("rmlSettings_lastForcedCacheClearing", doCacheNeedsToBeCleared);
        }
        else
        {
            int lastCacheClearing = PlayerPrefs.GetInt("rmlSettings_lastForcedCacheClearing");
            if (lastCacheClearing < doCacheNeedsToBeCleared)
            {
                ForceCacheClearing();
                PlayerPrefs.SetInt("rmlSettings_lastForcedCacheClearing", doCacheNeedsToBeCleared);
            }
        }

        StartCoroutine(LoadRML());
    }

    public void ForceCacheClearing()
    {
        foreach (string file in Directory.GetFiles(HLib.path_cacheFolder_mods))
        {
            if (file.EndsWith(".dll"))
            {
                File.Delete(file);
            }
        }
    }

    public void Update()
    {
        TemporaryPacketSplitingReadingClass.TemporaryPacketSplitingReading();
    }

    [Serializable]
    private class ServerVersionInfo
    {
        public string clientVersion = "";
    }

    IEnumerator LoadRML()
    {
        Directory.GetFiles(HLib.path_cacheFolder_temp).Where(s => s.ToLower().EndsWith(".dll")).ToList().ForEach((s) => File.Delete(s));

        ComponentManager<RML_Main>.Value = this;
        AssetBundleCreateRequest bundleLoadRequest = AssetBundle.LoadFromFileAsync(Path.Combine(HLib.path_dataFolder, "rml.assets"));
        yield return bundleLoadRequest;

        HLib.bundle = bundleLoadRequest.assetBundle;
        if (HLib.bundle == null) { yield break; }

        HLib.missingTexture = HLib.bundle.LoadAsset<Sprite>("missing").texture;

        gameObject.AddComponent<RConsole>();
        while (!GameObject.Find("RConsole")) { yield return new WaitForEndOfFrame(); }
        ComponentManager<RSocket>.Value = gameObject.AddComponent<RSocket>();

        try
        {
            var harmony = new Harmony("hytekgames.raftmodloader");
            harmony.PatchAll(Assembly.GetExecutingAssembly());
        }
        catch (Exception e)
        {
            Debug.LogError(e.ToString());
        }

        ComponentManager<RawSharp>.Value = gameObject.AddComponent<RawSharp>();
        ComponentManager<SocketIOComponent>.Value = gameObject.AddComponent<SocketIOComponent>();
        ComponentManager<UnityMainThreadDispatcher>.Value = gameObject.AddComponent<UnityMainThreadDispatcher>();

        gameObject.AddComponent<CustomLoadingScreen>();

        var MenuAssetLoadRequest = HLib.bundle.LoadAssetAsync<GameObject>("RMLMainMenu");
        yield return MenuAssetLoadRequest;

        GameObject tempmenuobj = MenuAssetLoadRequest.asset as GameObject;
        GameObject mainmenu = Instantiate(tempmenuobj, gameObject.transform);
        ComponentManager<MainMenu>.Value = mainmenu.AddComponent<MainMenu>();

        var HNotifyLoadRequest = HLib.bundle.LoadAssetAsync<GameObject>("RMLNotificationSystem");
        yield return HNotifyLoadRequest;

        GameObject hnotifytempobj = HNotifyLoadRequest.asset as GameObject;
        GameObject hnotify = Instantiate(hnotifytempobj, gameObject.transform);
        ComponentManager<HNotify>.Value = hnotify.AddComponent<HNotify>();
        gameObject.AddComponent<RChat>();
    }
}