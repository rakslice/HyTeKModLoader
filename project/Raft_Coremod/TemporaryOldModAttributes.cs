﻿using System;
using UnityEngine;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
[Obsolete]
public class ModTitle : Attribute
{
    public string ModTitleAttribute { get; private set; }

    public ModTitle(string value)
    {
        this.ModTitleAttribute = value;
    }
}

[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
[Obsolete]
public class ModDescription : Attribute
{
    public string ModDescriptionAttribute { get; private set; }

    public ModDescription(string value)
    {
        this.ModDescriptionAttribute = value;
    }
}

[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
[Obsolete]
public class ModAuthor : Attribute
{
    public string ModAuthorAttribute { get; private set; }

    public ModAuthor(string value)
    {
        this.ModAuthorAttribute = value;
    }
}

[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
[Obsolete]
public class ModVersion : Attribute
{
    public string ModVersionAttribute { get; private set; }

    public ModVersion(string value)
    {
        this.ModVersionAttribute = value;
    }
}

[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
[Obsolete]
public class RaftVersion : Attribute
{
    public string RaftVersionAttribute { get; private set; }

    public RaftVersion(string value)
    {
        this.RaftVersionAttribute = value;
    }
}

[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
[Obsolete]
public class ModIconUrl : Attribute
{
    public string ModIconUrlAttribute { get; private set; }

    public ModIconUrl(string value)
    {
        this.ModIconUrlAttribute = value;
    }
}

[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
[Obsolete]
public class ModWallpaperUrl : Attribute
{
    public string ModWallpaperUrlAttribute { get; private set; }

    public ModWallpaperUrl(string value)
    {
        this.ModWallpaperUrlAttribute = value;
    }
}

[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
[Obsolete]
public class ModVersionCheckUrl : Attribute
{
    public string ModVersionCheckUrlAttribute { get; private set; }

    public ModVersionCheckUrl(string value)
    {
        this.ModVersionCheckUrlAttribute = value;
    }
}

[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
[Obsolete]
public class ModIsPermanent : Attribute
{
    public bool ModIsPermanentAttribute { get; private set; }

    public ModIsPermanent(bool value)
    {
        this.ModIsPermanentAttribute = value;
    }
}

